import numpy as np
def binom(trials=1, ctr=0.03, size=100000):
    if 1 in np.random.binomial(trials, ctr, size=size)[:21]:
        return 1
    else:
        return 0

def test_a_b(ctr_a, ctr_b):
    a, b = 0, 0
    for i in range(10):
        a += binom(ctr=ctr_a)
        b += binom(ctr=ctr_b)
    if (ctr_a > ctr_b) and (a > b):
        return 1
    return 0

def global_test(n, ctr_a, ctr_b):
    e = 0
    for i in range(n):
        e += test_a_b(ctr_a, ctr_b)
    return e/n

